# ext - Extract without thinking

### Setup

`Ext` can be accessed in multiple ways, the two I recommend are aliasing, or adding it to your path.

One option is to alias `Ext` in `~/.bashrc`, or another shell's configuration file.

`alias ext="~/.scripts/ext"`

You can also place it in your path, or symlink it inside your path.

### Help

Extract archives with `ext FILE`.

### Credit

The original ext was created by Luke Smith, and can be found here [github](https://github.com/LukeSmithxyz/voidrice/blob/master/.scripts/tools/extract).
