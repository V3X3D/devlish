# proj - Setup easy navigation to projects

---

## Setup

Since `proj` is used to change your directory you will want to alias it. This is because shell scripts are ran inside of subshells.

### Shell

Aliases need the source shell to function correctly, here is a bash example that you can add to your `~/.bashrc` file.

`alias proj="source ~/.scripts/proj.sh"`

. is short for the source command

`alias proj=". ~/.scripts/proj.sh"`

Reload your bashrc file and you can now run `proj`.

### Tmux

The following can be put in your `~/.tmux.conf` and will start the pane in the my-game's directory.

`send-keys '^[' 'C-l' 'i' 'proj my-game' 'C-m' 'C-l'`

Checkout my [tmux.conf](https://gitlab.com/CodeMythos/dotfiles/blob/master/tmux.conf) for an example.

## Help

Use the h flag

`proj h`

`./proj.sh h`
