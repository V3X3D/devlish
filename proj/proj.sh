#!/bin/sh

alphabetical=true

projDir="$HOME/.config/proj"
projList=""$projDir"/projects"

if [ -z "$EDITOR" ]; then EDITOR="vi"; fi; # Set vi if $EDITOR not set

listAndReturn() { \
  echo "Current projects at $1."
  List | nl
  read -rp "Select a project to go to: " number
  alias=$(cat "$1" | nl | grep -w  "$number" | awk '{print $2}')
  path=$(cat "$1" | nl | grep -w  "$number" | awk '{print $4}')
}

### Main Functions
Help() { \
  echo "-----How to use proj-----"

  echo "*Basics"
  echo "  Select a project:             'proj ALIAS' or 'proj r ALIAS'"
  echo "  List your projects:           'proj l'"
  echo "  Make new project alias:       'proj n ALIAS'"
  echo "  Manually edit projects:       'proj e'"
  echo "  Get help using proj:          'proj h'"
  echo ""
  echo "*Extras"
  echo "  Select a project (numbered):  'proj' or 'proj r'"
  echo ""
  echo "*Notes"
  echo "  Project alias' saved in "$projList""
}

Run() { \
  if [ -z "$1" ]; then
    listAndReturn "$projList"
    cd "$path"
  else
    path=$(cat "$projList" | grep "$1.*;" | awk '{print $3}')
    if [ "$path" != "" ]; then
      cd "$path"
    else
      echo "Error: Project not found."
    fi
  fi
}

Edit() { \
  "$EDITOR" "$projList"
}

New() { \
  if [ -z "$1" ]; then
    echo "No alias provided."
  else
    alias=$(cat "$projList" | awk '{print $1}')
    used=$(echo "$alias" | grep "$1")

    if [ -z "$used" ]; then
      echo ""$1" ; "$(pwd)"" >> "$projList"
      echo "Directory "$(pwd)" set to alias "$1""
    else
      echo "Alias already used, try again with new alias"
    fi
  fi
}

List() {
  if [ "$alphabetical" = true ]; then
    cat "$projList" | sort | column -t
  else
    cat "$projList" | column -t
  fi
}

case "$1" in
  r) Run $2  ;;
  l) List    ;;
  e) Edit $2 ;;
  n) New $2  ;;
  h) Help    ;;
  *) Run $1  ;;
esac
