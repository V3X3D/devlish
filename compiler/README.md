# Compiler - Compile without thinking

### Setup Vim
`Compiler` can be accessed in multiple ways, the best in my opinion is to interface directly with your editor.

I'm using `compiler` with Vim and the following line added in my vimrc.

`map <leader>c :!~/.scripts/compiler <c-r>%<CR>`

Now the key combo of `<leader>c` compiles and runs the current document.

### Setup Environment
To get access to `compiler` alias it in bash `~/.bashrc`, or another shell.

`alias compiler="~/.scripts/compiler"`

You can also place it in your path, or symlink it inside your path.

### Help

Compile and run with `compiler FILE`.

If Compiler is not working on your document type then you will likely need to define that specific type inside of the script. The script is highly comprehensible so adding extra functionality won't be a burden.

### Credit

This is a script created my Luke Smith, and can be found here [github](https://github.com/LukeSmithxyz/voidrice/blob/master/.scripts/tools/compiler).
