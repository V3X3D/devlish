# Ifinstalled - Verify a program is installed

### Setup

Add ifinstalled to your path,
To get access to `ifinstalled` alias it in bash `~/.bashrc`, or another shells configuration file.

`alias ifinstalled="~/.scripts/ifinstalled"`

You can also place it in your path (recommended), or symlink it inside your path.

### Help

This is a dependency of many scripts of my scripts, for example use cases check other `devlish` scripts source code.

### Credit

The original of this script was created my Luke Smith, and can be found here [github](https://github.com/LukeSmithxyz/voidrice/blob/archi3/.local/bin/tools/ifinstalled).
