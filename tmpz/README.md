# Tmpz - Clone predefined directories (like snippets)

### Setup

Tmpz can be accessed in multiple ways, the two I recommend are aliasing, and adding it to your path.

One option is to alias it in bashes `~/.bashrc`, or another shells configuration file.

`alias tmpz="~/.scripts/tmpz.sh"`

You can also place it in your path, or symlink it inside your path.

### Help

Use the h flag

`tmpz -h`
