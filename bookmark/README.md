# bookmark - Navigate important directories quickly

### Dependencies
- [ifinstalled](https://gitlab.com/CodeMythos/devlish/tree/master/ifinstalled).
- [FZF](https://github.com/junegunn/fzf)

### Setup
Since `bookmark` is used to change your directory you will need to alias it. Shell scripts run in sub shells causing directory changes not to effect your main shell, but aliases work around this limitation by being bound to a call prefixed with "source".

Here is a bash example that you can add to your `~/.bashrc` file.

`alias bookmark="source ~/.scripts/bookmark"`

Reload your `~/.bashrc` file with `source ~/.bashrc` and you can now run `bookmark`.

### Help
For a list of commands get help with the -h flag.

`bookmark -h`

Also checkout the `example-bm-file`, for how you might want to tag bookmarks to help search for them with FZF.
