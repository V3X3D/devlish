# Devlish - A delicious bundle of development scripts

### What is Devlish?

`Devlish` is a set of POSIX shell scripts that speed up development.
Each script has a unique use case so you can pick and choose what works for you.

Be sure to checkout the `README.md` for each script for a clear view of what it does, how to get it working, dependencies, etc.

### Overview

`tmpz` Copies file structure templates that you define, like snippets for files and folders.

`bookmark` Save directories as bookmarks and then use FZF to quickly jump to them.

`ext` Extract files with ease.

`compiler` Compile everything with one command.

### Dependencies

Some scripts may depend on one or more of the following, check individual script `README.md` for more info.

`ifinstalled` Checks if a program is installed.

`FZF` A fuzzy finder, a must have program.

### Deprecated

Deprecated scripts are no longer part of my workflow, but are still fully functional.

`proj`
Set directories as projects, then easily jump to them (replaced by `bookmark`).

#### Notes

Some scripts are just modifications of other scripts I have found. Check the `README.md` in each scripts folder for more information.
